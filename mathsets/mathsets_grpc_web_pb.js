/**
 * @fileoverview gRPC-Web generated client stub for mathsets
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!


/* eslint-disable */
// @ts-nocheck



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.mathsets = require('./mathsets_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.mathsets.mathSetServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.mathsets.mathSetServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.UserConfigsRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodDescriptor_mathSetService_GetAvailableConfigs = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/GetAvailableConfigs',
  grpc.web.MethodType.UNARY,
  proto.mathsets.UserConfigsRequest,
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.UserConfigsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.UserConfigsRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodInfo_mathSetService_GetAvailableConfigs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.UserConfigsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @param {!proto.mathsets.UserConfigsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.ConfigResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.ConfigResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.getAvailableConfigs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/GetAvailableConfigs',
      request,
      metadata || {},
      methodDescriptor_mathSetService_GetAvailableConfigs,
      callback);
};


/**
 * @param {!proto.mathsets.UserConfigsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.ConfigResponse>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.getAvailableConfigs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/GetAvailableConfigs',
      request,
      metadata || {},
      methodDescriptor_mathSetService_GetAvailableConfigs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodDescriptor_mathSetService_AddNewConfig = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/AddNewConfig',
  grpc.web.MethodType.UNARY,
  proto.mathsets.ConfigRequest,
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodInfo_mathSetService_AddNewConfig = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.ConfigResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.ConfigResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.addNewConfig =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/AddNewConfig',
      request,
      metadata || {},
      methodDescriptor_mathSetService_AddNewConfig,
      callback);
};


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.ConfigResponse>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.addNewConfig =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/AddNewConfig',
      request,
      metadata || {},
      methodDescriptor_mathSetService_AddNewConfig);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.CreateSetRequest,
 *   !proto.mathsets.MathSet>}
 */
const methodDescriptor_mathSetService_CreateMathSet = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/CreateMathSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.CreateSetRequest,
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.CreateSetRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.CreateSetRequest,
 *   !proto.mathsets.MathSet>}
 */
const methodInfo_mathSetService_CreateMathSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.CreateSetRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @param {!proto.mathsets.CreateSetRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.MathSet)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.MathSet>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.createMathSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/CreateMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_CreateMathSet,
      callback);
};


/**
 * @param {!proto.mathsets.CreateSetRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.MathSet>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.createMathSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/CreateMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_CreateMathSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSetListResponse>}
 */
const methodDescriptor_mathSetService_ListMathSets = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/ListMathSets',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.MathSetListResponse,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSetListResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSetListResponse>}
 */
const methodInfo_mathSetService_ListMathSets = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.MathSetListResponse,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSetListResponse.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.MathSetListResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.MathSetListResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.listMathSets =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/ListMathSets',
      request,
      metadata || {},
      methodDescriptor_mathSetService_ListMathSets,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.MathSetListResponse>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.listMathSets =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/ListMathSets',
      request,
      metadata || {},
      methodDescriptor_mathSetService_ListMathSets);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_mathSetService_DestroyMathSet = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/DestroyMathSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_mathSetService_DestroyMathSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.destroyMathSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/DestroyMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_DestroyMathSet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.destroyMathSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/DestroyMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_DestroyMathSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_mathSetService_UpdateMathSet = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/UpdateMathSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_mathSetService_UpdateMathSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.updateMathSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/UpdateMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_UpdateMathSet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.updateMathSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/UpdateMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_UpdateMathSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSet>}
 */
const methodDescriptor_mathSetService_GetMathSet = new grpc.web.MethodDescriptor(
  '/mathsets.mathSetService/GetMathSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSet>}
 */
const methodInfo_mathSetService_GetMathSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.MathSet)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.MathSet>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.mathSetServiceClient.prototype.getMathSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.mathSetService/GetMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_GetMathSet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.MathSet>}
 *     Promise that resolves to the response
 */
proto.mathsets.mathSetServicePromiseClient.prototype.getMathSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.mathSetService/GetMathSet',
      request,
      metadata || {},
      methodDescriptor_mathSetService_GetMathSet);
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.mathsets.dbConnectorServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.mathsets.dbConnectorServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'text';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_CreateConfig = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/CreateConfig',
  grpc.web.MethodType.UNARY,
  proto.mathsets.ConfigRequest,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_CreateConfig = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.createConfig =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/CreateConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_CreateConfig,
      callback);
};


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.createConfig =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/CreateConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_CreateConfig);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.UserConfigsRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodDescriptor_dbConnectorService_ReadUserConfigs = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/ReadUserConfigs',
  grpc.web.MethodType.UNARY,
  proto.mathsets.UserConfigsRequest,
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.UserConfigsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.UserConfigsRequest,
 *   !proto.mathsets.ConfigResponse>}
 */
const methodInfo_dbConnectorService_ReadUserConfigs = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.ConfigResponse,
  /**
   * @param {!proto.mathsets.UserConfigsRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.ConfigResponse.deserializeBinary
);


/**
 * @param {!proto.mathsets.UserConfigsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.ConfigResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.ConfigResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.readUserConfigs =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/ReadUserConfigs',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ReadUserConfigs,
      callback);
};


/**
 * @param {!proto.mathsets.UserConfigsRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.ConfigResponse>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.readUserConfigs =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/ReadUserConfigs',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ReadUserConfigs);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_UpdateConfig = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/UpdateConfig',
  grpc.web.MethodType.UNARY,
  proto.mathsets.ConfigRequest,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.ConfigRequest,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_UpdateConfig = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigRequest} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.updateConfig =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/UpdateConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_UpdateConfig,
      callback);
};


/**
 * @param {!proto.mathsets.ConfigRequest} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.updateConfig =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/UpdateConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_UpdateConfig);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.ConfigInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_DestroyConfig = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/DestroyConfig',
  grpc.web.MethodType.UNARY,
  proto.mathsets.ConfigInformation,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.ConfigInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_DestroyConfig = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.ConfigInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.ConfigInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.destroyConfig =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/DestroyConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_DestroyConfig,
      callback);
};


/**
 * @param {!proto.mathsets.ConfigInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.destroyConfig =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/DestroyConfig',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_DestroyConfig);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.MathSet,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_CreateSet = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/CreateSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.MathSet,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.MathSet} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.MathSet,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_CreateSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.MathSet} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.MathSet} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.createSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/CreateSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_CreateSet,
      callback);
};


/**
 * @param {!proto.mathsets.MathSet} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.createSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/CreateSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_CreateSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSetListResponse>}
 */
const methodDescriptor_dbConnectorService_ListSets = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/ListSets',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.MathSetListResponse,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSetListResponse.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSetListResponse>}
 */
const methodInfo_dbConnectorService_ListSets = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.MathSetListResponse,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSetListResponse.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.MathSetListResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.MathSetListResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.listSets =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/ListSets',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ListSets,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.MathSetListResponse>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.listSets =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/ListSets',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ListSets);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSet>}
 */
const methodDescriptor_dbConnectorService_ReadSet = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/ReadSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.MathSet>}
 */
const methodInfo_dbConnectorService_ReadSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.MathSet,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.MathSet.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.MathSet)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.MathSet>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.readSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/ReadSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ReadSet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.MathSet>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.readSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/ReadSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_ReadSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_UpdateSet = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/UpdateSet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_UpdateSet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.updateSet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/UpdateSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_UpdateSet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.updateSet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/UpdateSet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_UpdateSet);
};


/**
 * @const
 * @type {!grpc.web.MethodDescriptor<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodDescriptor_dbConnectorService_DestroySet = new grpc.web.MethodDescriptor(
  '/mathsets.dbConnectorService/DestroySet',
  grpc.web.MethodType.UNARY,
  proto.mathsets.SetInformation,
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.mathsets.SetInformation,
 *   !proto.mathsets.StatusMessage>}
 */
const methodInfo_dbConnectorService_DestroySet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.mathsets.StatusMessage,
  /**
   * @param {!proto.mathsets.SetInformation} request
   * @return {!Uint8Array}
   */
  function(request) {
    return request.serializeBinary();
  },
  proto.mathsets.StatusMessage.deserializeBinary
);


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.mathsets.StatusMessage)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.mathsets.StatusMessage>|undefined}
 *     The XHR Node Readable Stream
 */
proto.mathsets.dbConnectorServiceClient.prototype.destroySet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/mathsets.dbConnectorService/DestroySet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_DestroySet,
      callback);
};


/**
 * @param {!proto.mathsets.SetInformation} request The
 *     request proto
 * @param {?Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.mathsets.StatusMessage>}
 *     Promise that resolves to the response
 */
proto.mathsets.dbConnectorServicePromiseClient.prototype.destroySet =
    function(request, metadata) {
  return this.client_.unaryCall(this.hostname_ +
      '/mathsets.dbConnectorService/DestroySet',
      request,
      metadata || {},
      methodDescriptor_dbConnectorService_DestroySet);
};


module.exports = proto.mathsets;

