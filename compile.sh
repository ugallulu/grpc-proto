echo "--- mathsets ---"

rm ./mathsets/*.go
rm ./mathsets/*.js

protoc -I=./mathsets --go_out=./mathsets --go_opt=paths=source_relative --go-grpc_out=./mathsets --go-grpc_opt=paths=source_relative ./mathsets/mathsets.proto

protoc -I=./mathsets --grpc-web_out=import_style=commonjs,mode=grpcwebtext:./mathsets ./mathsets/mathsets.proto
protoc -I=./mathsets --js_out=import_style=commonjs:./mathsets ./mathsets/mathsets.proto

echo "--- end ---"
